﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Haldur;
using H = Haldur;


namespace Haldur.Controllers
{
    public class TasksController : Controller
    {
        private HaldurEntities db = new HaldurEntities();

    
        //GET: Tasks
        public ActionResult Index(int? id, string sortOrder, string searchString, string sortDirection)


        {
           
            var tasks = db.Tasks.Include(t => t.Worker).Include(t => t.Assigner).AsEnumerable();

            ViewBag.Reverse = sortDirection == null || sortDirection == "" ? "" : "desc";

            User u = H.User.GetByEmail(User.Identity.Name);
            if (u == null) return RedirectToAction("Login", "Account");
            ViewBag.CurrentUser = u;
            ViewBag.SortParm = String.IsNullOrEmpty(sortOrder) ? "name" : sortOrder;

            if (u.IsInRole("ADM"))
            {
                tasks = db.Tasks.Include(t => t.Worker).Include(t => t.Assigner);
            }
            else
            {
                ViewBag.UserId = u.UserID;
                tasks = tasks.Where(t => t.AssignedTo == u.UserID || t.AssignedBy == u.UserID);
            }

            //ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "name";
            ViewBag.DateSortParmStart = sortOrder == "Date" ? "date_descstart" : "Date_start";
            ViewBag.DateSortParmEnd = sortOrder == "Date" ? "date_descend" : "Date_end";
            if (!String.IsNullOrEmpty(searchString))
            {
                tasks = tasks.Where(s => s.Project.ProjectName.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "taskname":
                    tasks = tasks.OrderBy(s => s.TaskName);
                    break;
                case "state":
                    tasks = tasks.OrderBy(s => s.State);
                    break;
                //case "name":
                //    tasks = tasks.OrderBy(s => s.State);
                //    tasks = tasks.OrderBy(s => s.TaskName);
                //    ViewBag.NameSortParm = "name_desc";
                //    break;
                case "Date_start":
                    tasks = tasks.OrderBy(p => p.StartDate);
                    break;
                case "date_descstart":
                    tasks = tasks.OrderByDescending(s => s.StartDate);
                    break;
                case "Date_end":
                    tasks = tasks.OrderBy(p => p.EndDate);
                    break;
                case "date_descend":
                    tasks = tasks.OrderByDescending(s => s.EndDate);
                    break;
                default:
                    tasks = tasks.OrderBy(p => p.StartDate);
                    break;
            }
            if (sortOrder == "desc") tasks = tasks.Reverse();
            return View(tasks.ToList());

        }
        //GET: Tasks/Details/5

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // GET: Tasks/Create
        public ActionResult Create(int? id)
        {
            User u = H.User.GetByEmail(User.Identity.Name);
            if (u == null) return RedirectToAction("Login", "Account");
            if (id.HasValue)
            {

                ViewBag.ProjectNumber = id;
                ViewBag.ProjectID = new SelectList(db.Projects.Where(x => x.UserID == u.UserID), "ProjectID", "ProjectName", id.Value);
                //ViewBag.AssignedTo = new SelectList(db.Users.Where(x => x.UserProjectRoles.Select(y => y.ProjectID).Contains(id.Value)));
            }
            else
            {
                ViewBag.ProjectNumber = null;
                ViewBag.ProjectID = new SelectList(db.Projects.Where(x => x.UserID  == u.UserID), "ProjectID", "ProjectName");

            }

            ViewBag.AssignedBy = new SelectList(db.Users, "UserID", "UserName");
            ViewBag.AssignedTo = new SelectList(db.Users, "UserID", "UserName");
            ViewBag.State = new SelectList(db.Tasks, "State", "State");
            if (User.Identity.IsAuthenticated)
            {
                Task t = new Task { AssignedBy = H.User.GetByEmail(User.Identity.Name).UserID };
                ViewBag.AssignedBy = new SelectList(db.Users, "UserID", "UserName", t.AssignedBy);
                ViewBag.Assigner = H.User.GetByEmail(User.Identity.Name).UserName;
                return View(t);
            }
            else return RedirectToAction("Login", "Account");

        }



        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TaskID,ProjectID,TaskSequence,TaskName,AssignedBy,AssignedTo,StartDate,EndDate,State,Comment")] Task task)
        {
            if (ModelState.IsValid)
            {
                db.Tasks.Add(task);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProjectID = new SelectList(db.Projects, "ProjectID", "ProjectName", task.ProjectID);
            ViewBag.AssignedBy = new SelectList(db.Users, "UserID", "UserName", task.AssignedBy);
            ViewBag.AssignedTo = new SelectList(db.Users, "UserID", "UserName", task.AssignedTo);
            return View(task);
        }

        // GET: Tasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.ProjectID = new SelectList(db.Projects, "ProjectID", "ProjectName", task.ProjectID);
                ViewBag.AssignedBy = new SelectList(db.Users, "UserID", "UserName", task.AssignedBy);
                ViewBag.AssignedTo = new SelectList(db.Users, "UserID", "UserName", task.AssignedTo);
               return View(task);

            }
            else return RedirectToAction("Login", "Account");
        }

        // POST: Tasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TaskID,ProjectID,TaskSequence,TaskName,AssignedBy,AssignedTo,StartDate,EndDate,State,Comment")] Task task)
        {
            if (ModelState.IsValid)
            {
                db.Entry(task).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProjectID = new SelectList(db.Projects, "ProjectID", "ProjectName", task.ProjectID);
            ViewBag.AssignedBy = new SelectList(db.Users, "UserID", "UserName", task.AssignedBy);
            ViewBag.AssignedTo = new SelectList(db.Users, "UserID", "UserName", task.AssignedTo);
            return View(task);
        }

        // GET: Tasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            if (User.Identity.IsAuthenticated)
            {
                return View(task);
            }
            else return RedirectToAction("Login", "Account");
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Task task = db.Tasks.Find(id);
            db.Tasks.Remove(task);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
