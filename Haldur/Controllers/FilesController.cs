﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Haldur;
using System.IO;

namespace Haldur
{
   partial class File
    {
        public string FileNameShort => FileName.Split('\\').Reverse().Take(1).SingleOrDefault();
    }
}


namespace Haldur.Controllers
{
    public class FilesController : Controller
    {
        private HaldurEntities db = new HaldurEntities();

        public ActionResult FileImage(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            File c = db.Files.Find(id.Value);
            if (c == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (c.Content?.Length == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var picture = c.Content.ToArray();
            if (picture[0] == 21) picture = picture.Skip(78).ToArray();
            return File(picture,  c.FileType ?? "image/jpg");
        }

        // GET: Files
        public ActionResult Index(string sortOrder, string searchString, string sortDirection)
        {
           
            var files = db.Files.Include(f => f.Project).Include(f => f.Task).AsEnumerable();

            ViewBag.Reverse = sortDirection == null || sortDirection == "" ? "" : "desc";
            ViewBag.SortParm = String.IsNullOrEmpty(sortOrder) ? "name" : sortOrder;
            //ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "name";

            if (!String.IsNullOrEmpty(searchString))
            {
                files = files.Where(s => s.Project.ProjectName.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "projectname":
                    files = files.OrderBy(s => s.Project.ProjectName);
                    break;
                case "taskname":
                    files = files.OrderBy(s => s.Task.TaskName);
                    break;
            }
            if (sortOrder == "desc") files = files.Reverse();
            return View(files.ToList());
        }




        // GET: Files/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            File file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
            
        }

        // GET: Files/Create
        public ActionResult Create()
        {
            ViewBag.ProjectID = new SelectList(db.Projects, "ProjectID", "ProjectName");
            ViewBag.TaskID = new SelectList(db.Tasks, "TaskID", "TaskName");
            return View();
        }

        // POST: Files/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FileID,FileName,FileType,ProjectID,TaskID,Content")] File file)
        {
            if (ModelState.IsValid)
            {
                db.Files.Add(file);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProjectID = new SelectList(db.Projects, "ProjectID", "ProjectName", file.ProjectID);
            ViewBag.TaskID = new SelectList(db.Tasks, "TaskID", "TaskName", file.TaskID);
            return View(file);
        }

        // GET: Files/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            File file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProjectID = new SelectList(db.Projects, "ProjectID", "ProjectName", file.ProjectID);
            ViewBag.TaskID = new SelectList(db.Tasks, "TaskID", "TaskName", file.TaskID);
            return View(file);
        }

        // POST: Files/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FileID,FileName,FileType,ProjectID,TaskID")] File sisu, HttpPostedFileBase file = null)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sisu).State = EntityState.Modified;
                db.Entry(sisu).Property(x => x.Content).IsModified = false;
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        sisu.Content = buff;
                        sisu.FileName = file.FileName;
                        sisu.FileType = file.ContentType;
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProjectID = new SelectList(db.Projects, "ProjectID", "ProjectName", sisu.ProjectID);
            ViewBag.TaskID = new SelectList(db.Tasks, "TaskID", "TaskName", sisu.TaskID);
            return View(sisu);
        }

        // GET: Files/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            File file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        // POST: Files/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            File file = db.Files.Find(id);
            db.Files.Remove(file);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
