﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using H = Haldur;

namespace Haldur.Controllers
{
    public class HomeController : Controller
    {
        static HaldurEntities db = new HaldurEntities();

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                User u = H.User.GetByEmail(User.Identity.Name);
                if (u == null) return RedirectToAction("Login", "Account");
                ViewBag.CurrentUser = u;


                var tasks = db.Tasks.ToList();
                ViewBag.tasksEndDate = db.Tasks.Where(t => t.AssignedTo == u.UserID).OrderByDescending(x => x.EndDate.Value).Take(3).ToList();
                ViewBag.tasksStartDate = db.Tasks.Where(t => t.AssignedTo == u.UserID).OrderByDescending(x => x.StartDate.Value).Take(3).ToList();
                return View(tasks);
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Projects()
        {
            ViewBag.Message = "Your projects.";

            return View();
        }
    }
}