﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Haldur;
using H = Haldur;

namespace Haldur
{
    partial class UserProjectRole
    {
        public string EMail { get; set; }
    }
}


namespace Haldur.Controllers
{
    public class UserProjectRolesController : Controller
    {
        private HaldurEntities db = new HaldurEntities();

        // GET: UserProjectRoles
        public ActionResult Index(string sortOrder, string searchString)
        {
            var userProjectRoles = db.UserProjectRoles.Include(u => u.Project).Include(u => u.Role).Include(u => u.User);

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "name";

            if (!String.IsNullOrEmpty(searchString))
            {
                userProjectRoles = userProjectRoles.Where(s => s.Project.ProjectName.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    userProjectRoles = userProjectRoles.OrderByDescending(s => s.Project.ProjectName);
                    userProjectRoles = userProjectRoles.OrderByDescending(s => s.User.UserName);
                    break;
                case "name":
                    userProjectRoles = userProjectRoles.OrderBy(s => s.Project.ProjectName);
                    userProjectRoles = userProjectRoles.OrderBy(s => s.User.UserName);
                    ViewBag.NameSortParm = "name_desc";
                    break;
            }

            return View(userProjectRoles.ToList());
        }

        // GET: UserProjectRoles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProjectRole userProjectRole = db.UserProjectRoles.Find(id);
            if (userProjectRole == null)
            {
                return HttpNotFound();
            }
            return View(userProjectRole);
        }

        // GET: UserProjectRoles/Create
        public ActionResult Create(int? id)
        {
            User u = H.User.GetByEmail(User.Identity.Name);
            if (id.HasValue)
            {
                ViewBag.ProjectNumber = id;
                ViewBag.ProjectID = new SelectList(db.Projects.Where(s => s.UserID.Equals(u.UserID)), "ProjectID", "ProjectName", id.Value);
            }
            else
            {
                ViewBag.ProjectNumber = null;
                ViewBag.ProjectID = new SelectList(db.Projects.Where(s => s.UserID.Equals(u.UserID)), "ProjectID", "ProjectName");
            }
            
            ViewBag.RoleID = new SelectList(db.Roles, "ID", "Role1");
            ViewBag.UserID = new SelectList(db.Users, "UserID", "EMail");
            return View();
        }

        //public ActionResult Create()
        //{
        //    User u = H.User.GetByEmail(User.Identity.Name);
        //    ViewBag.ProjectID = new SelectList(db.Projects.Where(s => s.UserID.Equals(u.UserID)), "ProjectID", "ProjectName");
        //    ViewBag.RoleID = new SelectList(db.Roles, "ID", "Role1");
        //    ViewBag.UserID = new SelectList(db.Users, "UserID", "EMail");
        //    return View();
        //}

        // POST: UserProjectRoles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UprID,UserID,RoleID,ProjectID,EMail")] UserProjectRole userProjectRole)
        {
            if (ModelState.IsValid && userProjectRole.EMail != "")
            {
                User u = db.Users.Where(x => x.EMail == userProjectRole.EMail).SingleOrDefault();
                if (u == null)
                {
                    HaldurEntities h2 = new HaldurEntities();
                    u = db.Users.Add(new H.User() { EMail = userProjectRole.EMail, UserName = userProjectRole.EMail, LastName = "?", FirstName = "?" } );
                    db.SaveChanges();
                }
                userProjectRole.UserID = u.UserID;
                db.UserProjectRoles.Add(userProjectRole);
                db.SaveChanges();
                return RedirectToAction("Index", "Projects");
            }

            ViewBag.ProjectID = new SelectList(db.Projects, "ProjectID", "ProjectName", userProjectRole.ProjectID);
            ViewBag.RoleID = new SelectList(db.Roles, "ID", "Role1", userProjectRole.RoleID);
            ViewBag.UserID = new SelectList(db.Users, "UserID", "UserName", userProjectRole.UserID);
            return View("Index", "Projects");
        }

        // GET: UserProjectRoles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProjectRole userProjectRole = db.UserProjectRoles.Find(id);
            if (userProjectRole == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProjectID = new SelectList(db.Projects, "ProjectID", "ProjectName", userProjectRole.ProjectID);
            ViewBag.RoleID = new SelectList(db.Roles, "ID", "Role1", userProjectRole.RoleID);
            ViewBag.UserID = new SelectList(db.Users, "UserID", "UserName", userProjectRole.UserID);
            return View(userProjectRole);
        }

        // POST: UserProjectRoles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UprID,UserID,RoleID,ProjectID")] UserProjectRole userProjectRole)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userProjectRole).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProjectID = new SelectList(db.Projects, "ProjectID", "ProjectName", userProjectRole.ProjectID);
            ViewBag.RoleID = new SelectList(db.Roles, "ID", "Role1", userProjectRole.RoleID);
            ViewBag.UserID = new SelectList(db.Users, "UserID", "UserName", userProjectRole.UserID);
            return View(userProjectRole);
        }

        // GET: UserProjectRoles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProjectRole userProjectRole = db.UserProjectRoles.Find(id);
            if (userProjectRole == null)
            {
                return HttpNotFound();
            }
            return View(userProjectRole);
        }

        // POST: UserProjectRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserProjectRole userProjectRole = db.UserProjectRoles.Find(id);
            db.UserProjectRoles.Remove(userProjectRole);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
