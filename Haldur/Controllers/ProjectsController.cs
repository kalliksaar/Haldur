﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Haldur;
using H = Haldur; 

namespace Haldur.Controllers
{
    public class ProjectsController : Controller
    {
        private HaldurEntities db = new HaldurEntities();

        // GET: Projects
        public ActionResult Index(string sortOrder, string searchString, string sortDirection)
        {
           
            var projects = db.Projects.Include(p => p.User);
            ViewBag.Reverse = sortDirection == null || sortDirection == "" ? "" : "desc";

            User u = H.User.GetByEmail(User.Identity.Name);
            if (u == null) return RedirectToAction("Login", "Account");
            ViewBag.SortParm = String.IsNullOrEmpty(sortOrder) ? "name" : sortOrder;

            if (u.IsInRole("ADM"))
            {
                projects = db.Projects.Include(p => p.User);
            }

            else
            {
                projects = projects.Where(s => s.UserID.Equals(u.UserID));
            }
            
            ViewBag.DateSortParmStart = sortOrder == "Date" ? "date_descstart" : "Date_start";
            ViewBag.DateSortParmEnd = sortOrder == "Date" ? "date_descend" : "Date_end";  //MIKS DESCENDING EI TÖÖTA???

            if (!String.IsNullOrEmpty(searchString))
            {
                projects = projects.Where(s => s.ProjectName.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "projectname":
                    projects = projects.OrderBy(s => s.ProjectName);
                    break;
                case "state":
                    projects = projects.OrderBy(s => s.State);
                    break;
                case "Date_start":
                    projects = projects.OrderBy(p =>p.StartDate);
                    break;
                case "date_descstart":
                    projects = projects.OrderByDescending(s => s.StartDate);
                    break;
                case "Date_end":
                    projects = projects.OrderBy(p => p.EndDate);
                    break;
                case "date_descend":
                    projects = projects.OrderByDescending(s => s.EndDate);
                    break;
                //default:
                //    projects = projects.OrderBy(p => p.StartDate);
                    //break;
            }
            if (sortOrder == "desc") projects = projects.Reverse();
            return View(projects.ToList());
        }

            // GET: Projects/Details/5
            public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // GET: Projects/Create 
        public ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "UserID", "UserName");
           
            ViewBag.State = new SelectList(db.Projects, "State", "State");
            if (User.Identity.IsAuthenticated)
            {
                Project u = new Project { UserID = H.User.GetByEmail(User.Identity.Name).UserID };
                ViewBag.Owner = H.User.GetByEmail(User.Identity.Name).UserName;
                return View(u);
            }
            else return RedirectToAction("Login", "Account");
        }

        // POST: Projects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProjectID,ProjectName,StartDate,EndDate,State,UserID,Comment")] Project project)
        {
            if (ModelState.IsValid)
            {
                db.Projects.Add(project);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.Users, "UserID", "UserName", project.UserID);
            ViewBag.State = new SelectList(db.Projects, "State", "State", project.State);
            return View(project);
        }

        // GET: Projects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.UserID = new SelectList(db.Users, "UserID", "UserName", project.UserID);
                ViewBag.State = new SelectList(db.Projects, "State", "State", project.State);
                return View(project);
            }
            else return RedirectToAction("Login", "Account");
        }  



    // POST: Projects/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProjectID,ProjectName,StartDate,EndDate,State,UserID,Comment")] Project project)
        {
            if (ModelState.IsValid)
            {
                db.Entry(project).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.Users, "UserID", "UserName", project.UserID);
            ViewBag.State = new SelectList(db.Projects, "State", "State", project.State);
            return View(project);
        }

        // GET: Projects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            if (User.Identity.IsAuthenticated)
            {
                return View(project);
            }
            else return RedirectToAction("Login", "Account");

        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Project project = db.Projects.Find(id);
            db.Projects.Remove(project);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    internal class Owner
    {
        public int UserID { get; internal set; }
    }
}
