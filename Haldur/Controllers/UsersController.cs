﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Haldur;
using H = Haldur;


namespace Haldur
{
    partial class User
    {
        static HaldurEntities db = new HaldurEntities();
        public static User GetByEmail(string email)
            => db.Users.Where(x => x.EMail == email).SingleOrDefault();

        public bool IsInRole (string roleName)
               => this.UserProjectRoles    // vaatan kasutaja küljes olevaid rolle
                .Select(x => x.Role.Role1.ToLower()) // leian iga rolli nime
                .Contains(roleName.ToLower()); // kontrollin, kas küsitav roll on nende hulgas

        public string FullName => FirstName + " " + LastName;

        
    }


}

namespace Haldur.Controllers
{
    public class UsersController : Controller
    {
        private HaldurEntities db = new HaldurEntities();

        // GET: Users
        public ActionResult Index(string sortOrder, string searchString, string sortDirection)
        {
            var kasutajad = db.Users.AsEnumerable();

            ViewBag.Reverse = sortDirection == null || sortDirection == "" ? "" : "desc";

            User u = H.User.GetByEmail(User.Identity.Name);
            ViewBag.CurrentUser = u;
            ViewBag.SortParm = String.IsNullOrEmpty(sortOrder) ? "name" : sortOrder;

            if (!String.IsNullOrEmpty(searchString))
            {
                kasutajad = kasutajad.Where(s => s.EMail.Contains(searchString));
            }

            
            switch (sortOrder)
            {
                case "lastname":
                    kasutajad = kasutajad.OrderBy(s => s.LastName);
                    break;
                case "firstname":
                    kasutajad = kasutajad.OrderBy(s => s.FirstName);
                    break;
                case "username":
                    kasutajad = kasutajad.OrderBy(s => s.UserName);
                    break;
                case "email":
                    kasutajad = kasutajad.OrderBy(s => s.EMail);
                    break;
                case "org":
                    kasutajad = kasutajad.OrderBy(s => s.Company);
                    break;
            }
            if (sortOrder == "desc") kasutajad = kasutajad.Reverse();
            return View(kasutajad.ToList());
         }


        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            User u = new User { EMail = User.Identity.Name };
            return View(u);
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,UserName,FirstName,LastName,EMail,Phone,Company")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index", "Projects");
            }

            return  RedirectToAction("Index", "Projects");
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,UserName,FirstName,LastName,EMail,Phone,Company")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
