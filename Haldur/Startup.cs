﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Haldur.Startup))]
namespace Haldur
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
